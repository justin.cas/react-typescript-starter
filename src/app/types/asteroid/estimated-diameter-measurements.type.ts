export interface EstimatedDiameterValues {
  estimated_diameter_max: number;
  estimated_diameter_min: number;
}
