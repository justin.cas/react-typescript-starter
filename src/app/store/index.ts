import { Store, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware } from 'react-router-redux';
import { History } from 'history';
import { RootState, rootReducer } from 'app/reducers';

export function configureStore(history: History, initialState?: RootState): Store<RootState> {
  const middlewares = [routerMiddleware(history)];
  let middleware = applyMiddleware(...middlewares);

  if (process.env.NODE_ENV !== 'production') {
    middleware = composeWithDevTools(middleware);
  }

  const store = createStore(rootReducer as any, initialState as any, middleware) as Store<
    RootState
  >;

  if (module.hot) {
    module.hot.accept('app/reducers', () => {
      const nextReducer = require('app/reducers/index');
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}
