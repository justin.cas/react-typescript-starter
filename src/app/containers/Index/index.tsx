import * as React from 'react';
import { Dispatch } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Header } from 'app/components';
import { Store } from 'redux';

export namespace Index {
  export interface Props extends RouteComponentProps<void> {
    dispatch: Dispatch<Store<any>>;
  }
}

export class Index extends React.Component<Index.Props> {
  constructor(props: Index.Props, context?: any) {
    super(props, context);
  }

  render() {
    return (
      <div>
        <Header />
      </div>
    );
  }
}
